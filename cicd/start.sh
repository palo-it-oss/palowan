#!/bin/sh

echo 'Load frontend docker image...'

docker load -i palowans-app-front.zip |
  sed -n 's/^Loaded image ID: sha256:\([0-9a-f]*\).*/\1/p' | 
  xargs -i docker tag {} palowans-app-front:latest

echo 'Load backend docker image...'

docker load -i palowans-app-back.zip |
  sed -n 's/^Loaded image ID: sha256:\([0-9a-f]*\).*/\1/p' | 
  xargs -i docker tag {} palowans-app-back:latest

echo 'Run docker compose...'

docker compose -f docker-compose.yml --env-file .env up --build -d