import Client, { Directory, connect } from '@dagger.io/dagger'
// UTILS
/* import { logError } from './utils.js' */

// PROCESS
import { execSync } from 'child_process'
// JSON
/* import frontJsonPackage from '../frontend/package.json' assert { type: 'json' }
import backJsonPackage from '../backend/package.json' assert { type: 'json' } */
// SSH
import { NodeSSH } from 'node-ssh'
// ENV VARIABLES
import 'dotenv/config'

//--- INIT ---//

// ssh
const initOnHost = async () => {
  const nodeSSH = new NodeSSH()

  const ssh = await nodeSSH.connect({
    host: process.env['SSH_HOST'],
    username: process.env['SSH_USERNAME'],
    password: process.env['SSH_PASSWORD'],
    privateKeyPath: process.env['SSH_PRIVATE_KEY_PATH'],
  })

  await ssh.putFiles([
    {
      local: 'cicd/palowans-app-front.zip',
      remote: '/home/debian/palowans/palowans-app-front.zip',
    },
    {
      local: 'cicd/palowans-app-back.zip',
      remote: '/home/debian/palowans/palowans-app-back.zip',
    },
    {
      local: 'cicd/docker-compose.yml',
      remote: '/home/debian/palowans/docker-compose.yml',
    },
    {
      local: 'cicd/start.sh',
      remote: '/home/debian/palowans/start.sh',
    },
    {
      local: 'cicd/.env',
      remote: '/home/debian/palowans/.env',
    },
  ])

  await ssh.execCommand('cd ~/palowans && sh start.sh').then(function (result) {
    console.log('STDOUT: ' + result.stdout)
    console.log('STDERR: ' + result.stderr)
  })
}

// app
/* const appName = JsonPackage.name
const appVersion = JsonPackage.version
const gitBranch =
  process.env['GIT_BRANCH_NAME'] ||
  execSync('git branch --show-current').toString().trim() */

// registry
/* const registry = process.env['CONTAINER_REGISTRY'] || '' */

//--- DAGGER PIPELINES ---//
const buildImage = async (
  client: Client,
  directory: Directory,
  scope: 'front' | 'back'
) => {
  const app = client
    .container()
    .from('node:18.13.0-alpine3.17')
    .withWorkdir('/usr/app')
    .withDirectory('.', directory)
    .withExec(['npm', 'ci'])

  // Run formater
  await app.withExec(['npm', 'run', 'format:check']).exitCode()
  // Run linter
  await app.withExec(['npm', 'run', 'lint:check']).exitCode()
  // Run tests
  //await app.withExec(['npm', 'test', '--', '--watchAll=false']).exitCode()
  // Run audit for dependencies vulnerabilities check
  await app.withExec(['npm', 'audit', '--audit-level=high']).exitCode()

  // Build app
  const appBuild = app
    .withWorkdir('/usr/app/dist')
    .withExec(['npm', 'run', 'build:app'])

  if (scope === 'front') {
    const appImage = client
      .container()
      .from('nginx:stable-alpine3.17')
      .withWorkdir('/usr/app')
      .withDirectory('.', appBuild.directory(`/usr/app/dist`))
      .withFile('/etc/nginx/nginx.conf', app.file('./nginx.conf'))
    /* .withEntrypoint(['nginx', '-g', 'daemon off;']) */

    return appImage
  }
  // Build app image
  const appImage = client
    .container()
    .from('node:18.13.0-alpine3.17')
    .withWorkdir('/usr/app')
    .withDirectory('.', appBuild.directory('/usr/app/dist'))
    .withDirectory(
      './node_modules',
      appBuild.directory('/usr/app/node_modules')
    )
    .withEntrypoint(['node', 'main.js'])

  return appImage
}

connect(
  async (client: Client) => {
    // Dagger pipelines variables
    const frontDirectory = client.host().directory('./frontend', {
      exclude: ['node_modules/', 'dist', '.env'],
    })
    const backDirectory = client.host().directory('./backend', {
      exclude: ['node_modules/', 'dist', '.env'],
    })

    //--- BUILDING ---//

    // Start app container & installing dependencies

    const frontImage = await buildImage(client, frontDirectory, 'front')
    const backImage = await buildImage(client, backDirectory, 'back')

    //--- PUBLISHING TO REGISTRY ---//

    // waiting to be able to use a proper registry
    await frontImage.export(`cicd/palowans-app-front.zip`)
    await backImage.export(`cicd/palowans-app-back.zip`)

    if (process.env['NODE_ENV'] === 'development') {
      execSync('cd cicd && sh start.sh', { stdio: 'inherit' })
    } else await initOnHost()
  },
  { LogOutput: process.stderr }
)
