#!/bin/bash

# Update the package list
sudo apt update

# Upgrade the installed packages
sudo apt upgrade -y

# Install Snapd
sudo apt install snapd -y

# Install Certbot as a Snap package
sudo snap install core
sudo snap refresh core
sudo snap install --classic certbot
sudo ln -s /snap/bin/certbot /usr/bin/certbot

# Stopping running containers
docker compose stop

# Enabling let's encrypt certificates
sudo certbot certonly --standalone