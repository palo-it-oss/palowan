/// custom color logger
export const logError = (string: string) =>
  console.log('\x1b[41m\x1b[37m=> ERROR: %s\x1b[0m', string)
