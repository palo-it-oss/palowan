import { Injectable, Logger } from '@nestjs/common'
import { catchError, firstValueFrom } from 'rxjs'
// PROVIDERS
import { HttpService } from '@nestjs/axios'
import { ConfigService } from '@nestjs/config'
// TYPES
import { User } from '@microsoft/microsoft-graph-types'
import type { AxiosError } from 'axios'

@Injectable()
export class UserService {
  constructor(
    private readonly httpService: HttpService,
    private readonly configService: ConfigService
  ) {}

  private readonly logger = new Logger(UserService.name)
  private readonly graphAPIEndpointMe = this.configService.get('auth.graphEndpoint.me')

  async findProfile(accessToken: string): Promise<User> {
    if (!accessToken) throw 'No given access token!'
    const { data } = await firstValueFrom(
      this.httpService
        .get<User>(this.graphAPIEndpointMe, { headers: { Authorization: `Bearer ${accessToken}` } })
        .pipe(
          catchError((error: AxiosError) => {
            this.logger.error(error?.response?.data)
            throw 'User profile could not be fetch!'
          })
        )
    )
    return data
  }

  async findProfilePicture(accessToken: string): Promise<User> {
    if (!accessToken) throw 'No given access token!'
    const { data } = await firstValueFrom(
      this.httpService
        .get<User>(this.graphAPIEndpointMe + '/photo/$value', {
          headers: { Authorization: `Bearer ${accessToken}` }
        })
        .pipe(
          catchError((error: AxiosError) => {
            this.logger.error(error?.response?.data)
            throw 'User picture could not be fetch!'
          })
        )
    )
    return data
  }
}
