import { Test, TestingModule } from '@nestjs/testing'
// MODULES
import { HttpModule } from '@nestjs/axios'
// CONTROLLERS
import { UserController } from './user.controller'
// PROVIDERS
import { UserService } from './user.service'
import { ConfigService } from '@nestjs/config'

describe('UserController', () => {
  let controller: UserController

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      controllers: [UserController],
      providers: [UserService, ConfigService]
    }).compile()

    controller = module.get<UserController>(UserController)
  })

  it('should be defined', () => {
    expect(controller).toBeDefined()
  })
})
