import { Module } from '@nestjs/common'
// MODULES
import { HttpModule } from '@nestjs/axios'
// CONTROLLER
import { UserController } from './user.controller'
// SERVICE
import { UserService } from './user.service'

@Module({
  imports: [HttpModule],
  controllers: [UserController],
  providers: [UserService]
})
export class UserModule {}
