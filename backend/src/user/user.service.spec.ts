import { Test, TestingModule } from '@nestjs/testing'
// MODULES
import { HttpModule } from '@nestjs/axios'
// PROVIDERS
import { UserService } from './user.service'
import { ConfigService } from '@nestjs/config'

describe('UserService', () => {
  let service: UserService

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      providers: [UserService, ConfigService]
    }).compile()

    service = module.get<UserService>(UserService)
  })

  it('should be defined', () => {
    expect(service).toBeDefined()
  })
})
