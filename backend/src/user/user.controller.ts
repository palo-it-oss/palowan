import { Controller, Get, Session } from '@nestjs/common'
// SERVICES
import { UserService } from './user.service'

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get('id')
  async getId(@Session() session) {
    return {
      idTokenClaims: session.account.idTokenClaims
    }
  }

  @Get('profile')
  async getProfile(@Session() session) {
    return await this.userService.findProfile(session.accessToken)
  }

  @Get('profile/picture')
  async getPicture(@Session() session) {
    return await this.userService.findProfilePicture(session.accessToken)
  }
}
