import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import { ConfigService } from '@nestjs/config'
// LIBS
import { Logger } from 'nestjs-pino'
import * as session from 'express-session'

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    bufferLogs: true,
    cors: true
  })

  // ENV VARIABLES
  const configService = app.get(ConfigService)

  // API PREFIX PATH
  app.setGlobalPrefix('api')

  // SESSION
  app.use(
    session({
      secret: configService.get('EXPRESS_SESSION_SECRET'),
      resave: false,
      saveUninitialized: false,
      proxy: process.env.NODE_ENV === 'production' ? true : false,
      cookie: {
        httpOnly: true,
        secure: process.env.NODE_ENV === 'production' ? true : false // set this to true on production
      }
    })
  )

  // LOGGER
  app.useLogger(app.get(Logger))

  process.on('uncaughtException', (err) => {
    console.log(`Uncaught Exception: ${err.message}`, err.stack)
    process.exit(1)
  })

  await app.listen(process.env.PORT ? parseInt(process.env.PORT) : 7001, '0.0.0.0')

  console.log(`
  💾   Welcome on Palowans Backend Server`)
  console.log(`
  🚀  Listening on ${await app.getUrl()}
  
  `)
}
bootstrap()
