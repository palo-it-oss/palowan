# Michel Guerin

## Education

- Beaux Arts (ESADSE)
- DU Droit et marché de l'art
- EPSI

## Travail

- Artisan encadreur/galeriste
- Palo IT

## Loisir

- Escalade
- Impression 3D
- Gravure
- Photographie argentique
- Développement

## Famille

- Marie
- Mona
- Maël

## Maison

- Besançon
- Lille
- Lyon
