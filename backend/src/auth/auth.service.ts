import { Injectable, Logger } from '@nestjs/common'
import { catchError, firstValueFrom } from 'rxjs'
import { ResponseMode } from '@azure/msal-node'
// PROVIDERS
import { HttpService } from '@nestjs/axios'
// TYPES
import type { AxiosError } from 'axios'

@Injectable()
export class AuthService {
  constructor(private readonly httpService: HttpService) {}

  private readonly logger = new Logger(AuthService.name)

  /**
   * Prepares the auth code request parameters and initiates the first leg of auth code flow
   * @param req: Express request object
   * @param res: Express response object
   * @param next: Express next function
   * @param authCodeUrlRequestParams: parameters for requesting an auth code url
   * @param authCodeRequestParams: parameters for requesting tokens using auth code
   */
  async redirectToAuthCodeUrl(
    authCodeUrlRequestParams,
    authCodeRequestParams,
    msalInstance,
    session,
    cryptoProvider
  ) {
    // Generate PKCE Codes before starting the authorization flow
    const { verifier, challenge } = await cryptoProvider.generatePkceCodes()

    // Set generated PKCE codes and method as session vars
    session.pkceCodes = {
      challengeMethod: 'S256',
      verifier: verifier,
      challenge: challenge
    }

    /**
     * By manipulating the request objects below before each request, we can obtain
     * auth artifacts with desired claims. For more information, visit:
     * https://azuread.github.io/microsoft-authentication-library-for-js/ref/modules/_azure_msal_node.html#authorizationurlrequest
     * https://azuread.github.io/microsoft-authentication-library-for-js/ref/modules/_azure_msal_node.html#authorizationcoderequest
     **/
    session.authCodeUrlRequest = {
      ...authCodeUrlRequestParams,
      responseMode: ResponseMode.FORM_POST, // recommended for confidential clients
      codeChallenge: session.pkceCodes.challenge,
      codeChallengeMethod: session.pkceCodes.challengeMethod
    }

    session.authCodeRequest = {
      ...authCodeRequestParams,
      code: ''
    }

    const authCodeUrlResponse = await msalInstance
      .getAuthCodeUrl(session.authCodeUrlRequest)
      .catch((error) => {
        throw error
      })
    if (!authCodeUrlResponse) throw 'AuthCodeUrlResponse could not be fetch!'

    return { url: authCodeUrlResponse }
  }

  /**
   * Retrieves cloud discovery metadata from the /discovery/instance endpoint
   * @returns
   */
  async getCloudDiscoveryMetadata(authority) {
    const endpoint = 'https://login.microsoftonline.com/common/discovery/instance'

    const { data } = await firstValueFrom(
      this.httpService
        .get(endpoint, {
          params: {
            'api-version': '1.1',
            authorization_endpoint: `${authority}/oauth2/v2.0/authorize`
          }
        })
        .pipe(
          catchError((error: AxiosError) => {
            this.logger.error(error.response.data)
            throw 'CloudDiscoveryMetadata could not be fetch!'
          })
        )
    )
    return data
  }

  /**
   * Retrieves oidc metadata from the openid endpoint
   * @returns
   */
  async getAuthorityMetadata(authority) {
    const endpoint = `${authority}/v2.0/.well-known/openid-configuration`

    const { data } = await firstValueFrom(
      this.httpService.get(endpoint).pipe(
        catchError((error: AxiosError) => {
          this.logger.error(error.response.data)
          throw 'AuthorityMetadata could not be fetch!'
        })
      )
    )
    return data
  }
}
