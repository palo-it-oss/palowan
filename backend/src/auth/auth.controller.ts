import { Controller, Get, Post, Body, Redirect, Session, Logger } from '@nestjs/common'
// SERVICE
import { ConfigService } from '@nestjs/config'
import { AuthService } from './auth.service'
// LIBRARIES
import * as msal from '@azure/msal-node'
// DECORATORS
import { Public } from 'src/decorators/public.decorator'

@Controller('auth')
export class AuthController {
  //--- INIT ---//

  constructor(private readonly authService: AuthService, private configService: ConfigService) {}

  private readonly logger = new Logger(AuthController.name)
  private msalConfig = this.configService.get('auth.msalConfig')
  private scopes = this.configService.get('auth.scopes')
  private redirectUri = this.configService.get('auth.redirectUri')
  private postLogoutRedirectUri = this.configService.get('auth.postLogoutRedirectUri')
  private successRedirect = this.configService.get('auth.successRedirect')

  private readonly cryptoProvider = new msal.CryptoProvider()

  @Public()
  @Get('signin')
  async signin(@Session() session: Record<string, any>) {
    // init
    const options = {
      scopes: this.scopes,
      redirectUri: this.redirectUri,
      successRedirect: this.successRedirect
    }

    /**
     * MSAL Node library allows you to pass your custom state as state parameter in the Request object.
     * The state parameter can also be used to encode information of the app's state before redirect.
     * You can pass the user's state in the app, such as the page or view they were on, as input to this parameter.
     */
    const state = this.cryptoProvider.base64Encode(
      JSON.stringify({
        successRedirect: options.successRedirect || '/'
      })
    )

    const authCodeUrlRequestParams = {
      state: state,

      /**
       * By default, MSAL Node will add OIDC scopes to the auth code url request. For more information, visit:
       * https://docs.microsoft.com/azure/active-directory/develop/v2-permissions-and-consent#openid-connect-scopes
       */
      scopes: options.scopes || [],
      redirectUri: options.redirectUri
    }

    const authCodeRequestParams = {
      state: state,

      /**
       * By default, MSAL Node will add OIDC scopes to the auth code request. For more information, visit:
       * https://docs.microsoft.com/azure/active-directory/develop/v2-permissions-and-consent#openid-connect-scopes
       */
      scopes: options.scopes || [],
      redirectUri: options.redirectUri
    }

    /**
     * If the current msal configuration does not have cloudDiscoveryMetadata or authorityMetadata, we will
     * make a request to the relevant endpoints to retrieve the metadata. This allows MSAL to avoid making
     * metadata discovery calls, thereby improving performance of token acquisition process. For more, see:
     * https://github.com/AzureAD/microsoft-authentication-library-for-js/blob/dev/lib/msal-node/docs/performance.md
     */
    if (!this.msalConfig.auth.cloudDiscoveryMetadata || !this.msalConfig.auth.authorityMetadata) {
      const [cloudDiscoveryMetadata, authorityMetadata] = await Promise.all([
        this.authService.getCloudDiscoveryMetadata(this.msalConfig.auth.authority),
        this.authService.getAuthorityMetadata(this.msalConfig.auth.authority)
      ])

      this.msalConfig.auth.cloudDiscoveryMetadata = JSON.stringify(cloudDiscoveryMetadata)
      this.msalConfig.auth.authorityMetadata = JSON.stringify(authorityMetadata)
    }

    const msalInstance = this.getMsalInstance(this.msalConfig)

    // trigger the first leg of auth code flow
    return await this.authService.redirectToAuthCodeUrl(
      authCodeUrlRequestParams,
      authCodeRequestParams,
      msalInstance,
      session,
      this.cryptoProvider
    )
  }

  @Get('token')
  async acquireToken(@Session() session: Record<string, any>) {
    // init
    const options = {
      scopes: this.scopes
      /*     redirectUri: this.redirectUri,
      successRedirect: this.successRedirect */
    }

    try {
      const msalInstance = this.getMsalInstance(this.msalConfig)

      /**
       * If a token cache exists in the session, deserialize it and set it as the
       * cache for the new MSAL CCA instance. For more, see:
       * https://github.com/AzureAD/microsoft-authentication-library-for-js/blob/dev/lib/msal-node/docs/caching.md
       */
      if (session.tokenCache) {
        msalInstance.getTokenCache().deserialize(session.tokenCache)
      }

      const tokenResponse = await msalInstance.acquireTokenSilent({
        account: session.account,
        scopes: options.scopes || []
      })

      /**
       * On successful token acquisition, write the updated token
       * cache back to the session. For more, see:
       * https://github.com/AzureAD/microsoft-authentication-library-for-js/blob/dev/lib/msal-node/docs/caching.md
       */
      session.tokenCache = msalInstance.getTokenCache().serialize()
      session.accessToken = tokenResponse.accessToken
      session.idToken = tokenResponse.idToken
      session.account = tokenResponse.account

      return { message: 'token acquired with success!' }
    } catch (error) {
      if (error instanceof msal.InteractionRequiredAuthError) return await this.signin(session)
    }
  }

  @Public()
  @Post('redirect')
  @Redirect()
  async handleRedirect(@Session() session: Record<string, any>, @Body() body) {
    // init
    const options = {}

    if (!body || !body.state) throw 'Error: response not found'
    this.logger.log(session)
    this.logger.log(session.pkceCodes)
    const authCodeRequest = {
      ...session.authCodeRequest,
      code: body.code,
      codeVerifier: session.pkceCodes.verifier
    }

    const msalInstance = this.getMsalInstance(this.msalConfig)

    if (session.tokenCache) {
      msalInstance.getTokenCache().deserialize(session.tokenCache)
    }

    const tokenResponse = await msalInstance.acquireTokenByCode(authCodeRequest, body)

    session.tokenCache = msalInstance.getTokenCache().serialize()
    session.idToken = tokenResponse.idToken
    session.account = tokenResponse.account
    session.isAuthenticated = true

    const state = JSON.parse(this.cryptoProvider.base64Decode(body.state))
    if (!state.successRedirect) throw 'No successRedirect url!'

    return { url: state.successRedirect }
  }

  @Get('signout')
  async signout(@Session() session: Record<string, any>) {
    // init
    const options = {
      postLogoutRedirectUri: this.postLogoutRedirectUri
    }

    /**
     * Construct a logout URI and redirect the user to end the
     * session with Azure AD. For more information, visit:
     * https://docs.microsoft.com/azure/active-directory/develop/v2-protocols-oidc#send-a-sign-out-request
     */
    let logoutUri = `${this.msalConfig.auth.authority}/oauth2/v2.0/`

    if (options.postLogoutRedirectUri) {
      logoutUri += `logout?post_logout_redirect_uri=${options.postLogoutRedirectUri}`
    }

    session.destroy()

    return { url: logoutUri }
  }

  /**
   * Instantiates a new MSAL ConfidentialClientApplication object
   * @param msalConfig: MSAL Node Configuration object
   * @returns
   */
  getMsalInstance(msalConfig) {
    return new msal.ConfidentialClientApplication(msalConfig)
  }
}
