import { Module } from '@nestjs/common'
// MODULES
import { HttpModule } from '@nestjs/axios'
// CONTROLLERS
import { AuthController } from './auth.controller'
// SERVICE
import { AuthService } from './auth.service'
// GLOBAL GUARD
import { APP_GUARD } from '@nestjs/core'
import { AuthGuard } from './auth.guard'

@Module({
  imports: [HttpModule],
  controllers: [AuthController],
  providers: [
    AuthService,
    {
      provide: APP_GUARD,
      useClass: AuthGuard
    }
  ]
})
export class AuthModule {}
