import { Controller, Get } from '@nestjs/common'
import concatMd from 'concat-md'

@Controller('personal-maps')
export class PersonalMapsController {
  @Get('')
  async getPersonalMaps() {
    const path =
      process.env['NODE_ENV'] === 'production'
        ? './assets/personalMaps'
        : './src/assets/personalMaps'
    const personalMaps = await concatMd(path, {
      hideAnchorLinks: true,
      dirNameAsTitle: true,
      decreaseTitleLevels: true
    })

    return personalMaps
  }
}
