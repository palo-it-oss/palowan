import { Module } from '@nestjs/common'
import { PersonalMapsController } from './personalMaps.controller'

@Module({
  controllers: [PersonalMapsController]
})
export class PersonalMapsModule {}
