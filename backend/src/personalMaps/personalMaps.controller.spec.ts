import { Test, TestingModule } from '@nestjs/testing'
// MODULES
import { HttpModule } from '@nestjs/axios'
// CONTROLLERS
import { PersonalMapsController } from './personalMaps.controller'

describe('PersonalMapsController', () => {
  let controller: PersonalMapsController

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      controllers: [PersonalMapsController]
    }).compile()

    controller = module.get<PersonalMapsController>(PersonalMapsController)
  })

  it('should be defined', () => {
    expect(controller).toBeDefined()
  })
})
