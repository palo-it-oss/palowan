export default () => ({
  sessionSecret: process.env.EXPRESS_SESSION_SECRET,
  port: parseInt(process.env.PORT, 10) || 7001,
  paloOffices: [
    'paris',
    'toulouse',
    'nantes',
    'lyon',
    'bogota',
    'medellin',
    'sydney',
    'hong kong',
    'mexico',
    'singapore',
    'bangkok',
    'barcelona',
    'madrid',
    'new-york'
  ]
})
