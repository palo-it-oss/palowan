export default () => ({
  unsplash: {
    endpoint: {
      random: process.env.UNSPLASH_API + '/photos/random'
    },
    accessKey: process.env.UNSPLASH_ACCESS_KEY
  }
})
