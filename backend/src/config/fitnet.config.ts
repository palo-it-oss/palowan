export default () => ({
  fitnet: {
    auth: Buffer.from(process.env.FITNET_EMAIL + ':' + process.env.FITNET_PASSWORD).toString(
      'base64'
    ),
    baseUrl: process.env.FITNET_API
  }
})
