export default () => ({
  auth: {
    msalConfig: {
      auth: {
        clientId: process.env.MS_CLIENT_ID, // 'Application (client) ID' of app registration in Azure portal - this value is a GUID
        authority: process.env.MS_CLOUD_INSTANCE + process.env.MS_TENANT_ID, // Full directory URL, in the form of https://login.microsoftonline.com/<tenant>
        clientSecret: process.env.MS_CLIENT_SECRET, // Client secret generated from the app registration in Azure portal
        cloudDiscoveryMetadata: null,
        authorityMetadata: null
      },
      system: {
        loggerOptions: {
          loggerCallback(logLevel, message, containsPii) {
            console.log(message)
          },
          piiLoggingEnabled: false,
          logLevel: 2
        }
      }
    },
    redirectUri: process.env.MS_REDIRECT_URI,
    postLogoutRedirectUri: process.env.MS_POST_LOGOUT_REDIRECT_URI,
    successRedirect: process.env.MS_SUCCESS_REDIRECT,
    scopes: process.env.MS_SCOPES?.split(', '),
    graphEndpoint: {
      me: process.env.MS_GRAPH_API_ENDPOINT + 'v1.0/me'
    }
  }
})
