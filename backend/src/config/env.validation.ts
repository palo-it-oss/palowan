import * as Joi from 'joi'

export const validation = {
  schema: Joi.object({
    NODE_ENV: Joi.string().valid('development', 'production').default('development'),
    PORT: Joi.number().default(7001),
    MS_CLOUD_INSTANCE: Joi.string(),
    MS_TENANT_ID: Joi.string(),
    MS_CLIENT_ID: Joi.string(),
    MS_CLIENT_SECRET: Joi.string(),
    MS_REDIRECT_URI: Joi.string(),
    MS_POST_LOGOUT_REDIRECT_URI: Joi.string(),
    MS_SUCCESS_REDIRECT: Joi.string(),
    MS_GRAPH_API_ENDPOINT: Joi.string(),
    MS_SCOPES: Joi.string(),
    EXPRESS_SESSION_SECRET: Joi.string(),
    FITNET_API: Joi.string(),
    FITNET_EMAIL: Joi.string(),
    FITNET_PASSWORD: Joi.string(),
    UNSPLASH_API: Joi.string(),
    UNSPLASH_ACCESS_KEY: Joi.string()
  }),
  options: {
    allowUnknown: true,
    abortEarly: true
  }
}
