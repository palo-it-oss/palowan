import { Controller, Get } from '@nestjs/common'
import { Public } from 'src/decorators/public.decorator'

@Controller('health')
export class HealthController {
  @Public()
  @Get('')
  async health() {
    return { message: 'Ok', status: 200 }
  }
}
