import { Test, TestingModule } from '@nestjs/testing'
// MODULES
import { HttpModule } from '@nestjs/axios'
// PROVIDERS
import { FitnetService } from './fitnet.service'
import { ConfigService } from '@nestjs/config'

describe('FitnetService', () => {
  let service: FitnetService

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      providers: [FitnetService, ConfigService]
    }).compile()

    service = module.get<FitnetService>(FitnetService)
  })

  it('should be defined', () => {
    expect(service).toBeDefined()
  })
})
