import { Injectable, Logger } from '@nestjs/common'
import { catchError, firstValueFrom } from 'rxjs'
// PROVIDERS
import { HttpService } from '@nestjs/axios'
import { ConfigService } from '@nestjs/config'
// TYPES
import type { AxiosError } from 'axios'
import { Company, Profile, MonthlyStaffing, MonthlyStaffingParameters } from 'types/fitnet'

@Injectable()
export class FitnetService {
  constructor(
    private readonly httpService: HttpService,
    private readonly configService: ConfigService
  ) {}

  private readonly logger = new Logger(FitnetService.name)
  private readonly fitnetAPIEndpoint = this.configService.get<string>('fitnet.baseUrl')
  private readonly fitnetAuth = this.configService.get<string>('fitnet.auth')
  private readonly fitnetAPIHeaders = {
    headers: {
      Authorization: `Basic ${this.fitnetAuth}`
    }
  }

  async findCompanies() {
    const companiesEndpoint = this.fitnetAPIEndpoint + 'companies'
    const { data } = await firstValueFrom(
      this.httpService.get<Company[]>(companiesEndpoint, this.fitnetAPIHeaders).pipe(
        catchError((error: AxiosError) => {
          this.logger.error(error.response.data)
          throw 'Fitnet companies could not be fetch!'
        })
      )
    )

    const filteredOffices = data
      .filter((company) => company.name.startsWith('Palo'))
      .map((company) => ({ companyId: company.companyId, name: company.name.substring(8) }))
    return filteredOffices
  }

  async findProfiles() {
    const profilesEndpoint = this.fitnetAPIEndpoint + 'profiles'
    const { data } = await firstValueFrom(
      this.httpService.get<Profile[]>(profilesEndpoint, this.fitnetAPIHeaders).pipe(
        catchError((error: AxiosError) => {
          this.logger.error(error.response.data)
          throw 'Fitnet profiles could not be fetch!'
        })
      )
    )
    return data
  }

  async findMonthlyStaffingEstimated({ startDate, endDate, companyId }: MonthlyStaffingParameters) {
    const monthlyStaffingEndpoint =
      this.fitnetAPIEndpoint +
      `MonthlyStaffing/Estimated?startDate=${startDate}&endDate=${endDate}&excludeOpportunities=1&pipeline=0&companyId=${companyId}`

    const { data } = await firstValueFrom(
      this.httpService.get<MonthlyStaffing[]>(monthlyStaffingEndpoint, this.fitnetAPIHeaders).pipe(
        catchError((error: AxiosError) => {
          this.logger.error(error.response.data)
          throw 'Fitnet estimated staffing could not be fetch!'
        })
      )
    )
    return data
  }

  async findMonthlyStaffingReal({ startDate, endDate, companyId }: MonthlyStaffingParameters) {
    const monthlyStaffingEndpoint =
      this.fitnetAPIEndpoint +
      `MonthlyStaffing/Real?startDate=${startDate}&endDate=${endDate}&companyId=${companyId}`

    const { data } = await firstValueFrom(
      this.httpService.get<MonthlyStaffing[]>(monthlyStaffingEndpoint, this.fitnetAPIHeaders).pipe(
        catchError((error: AxiosError) => {
          this.logger.error(error.response.data)
          throw 'Fitnet real staffing could not be fetch!'
        })
      )
    )
    return data
  }

  async findMonthlyStaffingEstimatedByMonth({ startDate, companyId }: MonthlyStaffingParameters) {
    const monthlyStaffingEndpoint =
      this.fitnetAPIEndpoint + `MonthlyStaffing/Estimated/byCompany/${companyId}/${startDate}/1/0/1`

    const { data } = await firstValueFrom(
      this.httpService.get<MonthlyStaffing[]>(monthlyStaffingEndpoint, this.fitnetAPIHeaders).pipe(
        catchError((error: AxiosError) => {
          this.logger.error(error.response.data)
          throw 'Fitnet estimated staffing by month could not be fetch!'
        })
      )
    )
    return data
  }

  async findMonthlyStaffingRealByMonth({ startDate, companyId }: MonthlyStaffingParameters) {
    const monthlyStaffingEndpoint =
      this.fitnetAPIEndpoint + `MonthlyStaffing/Real/byCompany/${companyId}/${startDate}/1`

    const { data } = await firstValueFrom(
      this.httpService.get<MonthlyStaffing[]>(monthlyStaffingEndpoint, this.fitnetAPIHeaders).pipe(
        catchError((error: AxiosError) => {
          this.logger.error(error.response.data)
          throw 'Fitnet real staffing by month could not be fetch!'
        })
      )
    )
    return data
  }
}
