import { Module } from '@nestjs/common'
// MODULES
import { HttpModule } from '@nestjs/axios'
// CONTROLLER
import { FitnetController } from './fitnet.controller'
// SERVICES
import { FitnetService } from './fitnet.service'

@Module({
  imports: [HttpModule],
  controllers: [FitnetController],
  providers: [FitnetService]
})
export class FitnetModule {}
