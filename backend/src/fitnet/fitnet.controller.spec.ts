import { Test, TestingModule } from '@nestjs/testing'
// MODULES
import { HttpModule } from '@nestjs/axios'
// CONTROLLERS
import { FitnetController } from './fitnet.controller'
// PROVIDERS
import { FitnetService } from './fitnet.service'
import { ConfigService } from '@nestjs/config'

describe('FitnetController', () => {
  let controller: FitnetController

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      controllers: [FitnetController],
      providers: [FitnetService, ConfigService]
    }).compile()

    controller = module.get<FitnetController>(FitnetController)
  })

  it('should be defined', () => {
    expect(controller).toBeDefined()
  })
})
