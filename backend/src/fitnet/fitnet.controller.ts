import { Controller, Get, Query } from '@nestjs/common'
// SERVICES
import { FitnetService } from './fitnet.service'
// TYPES
import { AvailabilitiesQuery, MonthlyStaffingParameters } from 'types/fitnet'

@Controller('fitnet')
export class FitnetController {
  constructor(private readonly fitnetService: FitnetService) {}

  async getProductionProfilesByIds() {
    const profiles = await this.fitnetService.findProfiles()
    // filter profiles based on production ones
    return profiles.filter((profile) => !profile.offProdProfile).map((profile) => profile.profileId)
  }

  async getMonthlyStaffing({ startDate, endDate, companyId }: MonthlyStaffingParameters) {
    const currentMonth = new Date().getMonth() + 1
    const currentYear = new Date().getFullYear()
    const startDateArray = startDate.split('-')

    const isFetchingEstimated =
      currentMonth >= Number(startDateArray[0]) && currentYear >= Number(startDateArray[0])

    if (isFetchingEstimated)
      return await this.fitnetService.findMonthlyStaffingEstimated({
        startDate,
        endDate,
        companyId
      })
    else return await this.fitnetService.findMonthlyStaffingReal({ startDate, endDate, companyId })
  }

  async getMonthlyStaffingByMonth({ startDate, companyId }: MonthlyStaffingParameters) {
    const currentMonth = new Date().getMonth() + 1
    const startDateMonth = Number(startDate.split('-')[0])

    const isFetchingEstimated = currentMonth <= startDateMonth

    if (isFetchingEstimated)
      return await this.fitnetService.findMonthlyStaffingEstimatedByMonth({
        startDate,
        companyId
      })
    else return await this.fitnetService.findMonthlyStaffingRealByMonth({ startDate, companyId })
  }

  @Get('companies')
  async getCompanies() {
    return await this.fitnetService.findCompanies()
  }

  @Get('availability')
  async getPalowansAvailabilities(@Query() query: AvailabilitiesQuery) {
    // init
    const { startDate, endDate, companyId, onlyGardenView } = query

    const palowans = await this.getMonthlyStaffing({ startDate, endDate, companyId })

    const productionProfileIds = await this.getProductionProfilesByIds()
    // filter palowan by active & production profiles
    // In case of research of unstaffed/in the garden palowans : add those with a staffingRate under 50%
    const filteredPalowans = palowans
      .filter(
        (palowan) =>
          palowan.employeeStatus === 'active' && productionProfileIds.includes(palowan.profileId)
      )
      .map((palowan) => {
        return {
          employeeId: palowan.employeeId,
          fullName: palowan.fullName,
          companyName: palowan.companyName,
          companyId: palowan.companyId,
          staffingRate: Math.floor(palowan.staffingRate * 100),
          numberDaysOnContractActivities: palowan.numberDaysOnContractActivities,
          numberDaysOffContractActivities:
            palowan.numberDaysOffContractActivities +
            palowan.numberDaysOffContractActivitiesOnUnworkedDays,
          numberDaysInternalActivities: palowan.numberDaysInternalActivity
        }
      })

    // get the availability for a given month
    if (startDate === endDate)
      return filteredPalowans.filter((palowan) => palowan.staffingRate <= 0.5)

    // get the availability for a range of months
    const mergedData: typeof filteredPalowans = Object.values(
      filteredPalowans.reduce((accumulator, palowan) => {
        const key = palowan.employeeId
        if (!accumulator[key]) {
          accumulator[key] = { ...palowan, sum: palowan.staffingRate, iteration: 1 }
        } else {
          // Sum staffingRate + keep the iteration value to be able to calculate the average afterwards
          accumulator[key].sum += palowan.staffingRate
          accumulator[key].iteration++

          accumulator[key].numberDaysOnContractActivities += palowan.numberDaysOnContractActivities
          accumulator[key].numberDaysOffContractActivities +=
            palowan.numberDaysOffContractActivities
          accumulator[key].numberDaysInternalActivities += palowan.numberDaysInternalActivities
        }
        return accumulator
      }, {})
    )

    // calculate staffingRate average + delete unnecessary keys
    mergedData.forEach((obj) => {
      obj['staffingRate'] = Math.floor(obj['sum'] / obj['iteration'])
      delete obj['sum']
      delete obj['iteration']
    })

    if (onlyGardenView === 'true') {
      return mergedData.filter((palowan) => palowan.staffingRate <= 50)
    }
    return mergedData
  }

  @Get('availability/month')
  async getPalowansAvailabilitiesByMonth(@Query() query: AvailabilitiesQuery) {
    // init
    const { startDate, companyId, onlyGardenView } = query

    const palowans = await this.getMonthlyStaffingByMonth({ startDate, companyId })

    const productionProfileIds = await this.getProductionProfilesByIds()
    // filter palowan by active & production profiles
    // In case of research of unstaffed/in the garden palowans : add those with a staffingRate under 50%
    const filtered = palowans
      .filter((palowan) => {
        if (onlyGardenView === 'true')
          return (
            palowan.staffingRate <= 0.5 &&
            palowan.employeeStatus === 'active' &&
            productionProfileIds.includes(palowan.profileId)
          )
        return (
          palowan.employeeStatus === 'active' && productionProfileIds.includes(palowan.profileId)
        )
      })
      .map((palowan) => {
        return {
          employeeId: palowan.employeeId,
          fullName: palowan.fullName,
          companyName: palowan.companyName,
          companyId: palowan.companyId,
          staffingRate: Math.floor(palowan.staffingRate * 100),
          numberDaysOnContractActivities: palowan.numberDaysOnContractActivities,
          numberDaysOffContractActivities:
            palowan.numberDaysOffContractActivities +
            palowan.numberDaysOffContractActivitiesOnUnworkedDays,
          numberDaysInternalActivities: palowan.numberDaysInternalActivity
        }
      })
    return filtered
  }
}
