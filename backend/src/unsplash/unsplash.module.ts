import { Module } from '@nestjs/common'
// MODULES
import { HttpModule } from '@nestjs/axios'
// CONTROLLERS
import { UnsplashController } from './unsplash.controller'
// SERVICES
import { UnsplashService } from './unsplash.service'

@Module({
  imports: [HttpModule],
  controllers: [UnsplashController],
  providers: [UnsplashService]
})
export class UnsplashModule {}
