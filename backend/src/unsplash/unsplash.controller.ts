import { Controller, Get } from '@nestjs/common'
// SERVICES
import { ConfigService } from '@nestjs/config'
import { UnsplashService } from './unsplash.service'
// DECORATORS
import { Public } from 'src/decorators/public.decorator'

@Controller('unsplash')
export class UnsplashController {
  constructor(
    private readonly unsplashService: UnsplashService,
    private configService: ConfigService
  ) {}
  private paloOffices = this.configService.get('paloOffices')

  @Public()
  @Get('')
  async getImage() {
    const randomIndex = Math.floor(Math.random() * this.paloOffices.length)
    const selectedOffice = this.paloOffices[randomIndex]
    const response = await this.unsplashService.findRandomImage(selectedOffice)
    return {
      blurHash: response.blur_hash,
      url: response.urls.raw,
      office: selectedOffice,
      link: response.links.html + '?utm_source=palowans&utm_medium=referral', //required by unsplash api terms
      author: `${response.user.first_name} ${response.user.last_name}`
    }
  }
}
