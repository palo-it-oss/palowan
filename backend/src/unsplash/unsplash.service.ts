import { Injectable, Logger } from '@nestjs/common'
import { catchError, firstValueFrom } from 'rxjs'
// PROVIDERS
import { HttpService } from '@nestjs/axios'
import { ConfigService } from '@nestjs/config'
// TYPES
import type { AxiosError } from 'axios'

@Injectable()
export class UnsplashService {
  constructor(
    private readonly httpService: HttpService,
    private readonly configService: ConfigService
  ) {}

  private readonly logger = new Logger(UnsplashService.name)
  private readonly unsplashAccessKey = this.configService.get('unsplash.accessKey')
  private readonly unsplashAPIEndpointRandom = this.configService.get('unsplash.endpoint.random')

  async findRandomImage(office: string) {
    const { data } = await firstValueFrom(
      this.httpService
        .get(this.unsplashAPIEndpointRandom, {
          params: {
            query: office,
            content_filter: 'high'
          },
          headers: { Authorization: `Client-ID ${this.unsplashAccessKey}` }
        })
        .pipe(
          catchError((error: AxiosError) => {
            this.logger.error(error?.response?.data)
            throw 'Unsplash image could not be fetch!'
          })
        )
    )
    return data
  }
}
