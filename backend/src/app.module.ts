import { Module } from '@nestjs/common'
// NEST MODULES
import { ConfigModule } from '@nestjs/config'
import { LoggerModule } from 'nestjs-pino'
// CONFIG
import { loggerOptions } from './config'
import { validation } from './config/env.validation'
import globalConfig from './config/global.config'
import authConfig from './config/auth.config'
import fitnetConfig from './config/fitnet.config'
import unsplashConfig from './config/unsplash.config'
// CUSTOM MODULES
import { AuthModule } from './auth/auth.module'
import { PersonalMapsModule } from './personalMaps/personalMaps.module'
import { UserModule } from './user/user.module'
import { FitnetModule } from './fitnet/fitnet.module'
import { HealthModule } from './health/health.module'
import { UnsplashModule } from './unsplash/unsplash.module'

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      validationSchema: validation.schema,
      validationOptions: validation.options,
      load: [globalConfig, authConfig, fitnetConfig, unsplashConfig]
    }),
    LoggerModule.forRoot(loggerOptions),
    AuthModule,
    PersonalMapsModule,
    UserModule,
    FitnetModule,
    HealthModule,
    UnsplashModule
  ]
})
export class AppModule {}
