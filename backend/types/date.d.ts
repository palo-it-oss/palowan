type oneToNine = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9
type zeroToNine = 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9
/**
 * Years
 */
type YYYY = `20${zeroToNine}${zeroToNine}`
/**
 * Months
 */
type MM = `0${oneToNine}` | `1${0 | 1 | 2}`

/**
 * YYYYMMDD
 */
export type MonthYearString = `${MM}-${YYYY}`
