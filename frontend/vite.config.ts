import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import tailwindcss from 'tailwindcss'

// Backend target
const target = process.env.APP_PORT
  ? `http://localhost:${process.env.APP_PORT}`
  : 'http://localhost:7001'

// https://vitejs.dev/config/
export default defineConfig({
  base: '',
  plugins: [vue()],
  css: {
    postcss: {
      plugins: [tailwindcss]
    }
  },
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url)),
      types: fileURLToPath(new URL('../types', import.meta.url))
    }
  },
  server: {
    port: 7000,
    proxy: {
      '/api': {
        target,
        changeOrigin: true
        /* rewrite: (path) => path.replace(/^\/backend/, 'api') */
      }
    }
  }
})
