import type { MonthYearString } from './date.d.ts'

export type Company = { companyId: number; name: string }

export type AvailabilitiesQuery = {
  onlyGardenView: 'true' | 'false'
  startDate: MonthYearString
  endDate?: MonthYearString
  companyId: number
}

export type MonthlyStaffingParameters = {
  startDate: MonthYearString
  endDate: MonthYearString
  companyId: number
}

export type MonthlyStaffing = {
  employeeId: number
  fullName: string
  tenantId: string
  companyId: number
  companyName: string
  employeeName: string
  buId: null
  buName: null
  teamId: number
  teamName: string
  teamCode: string
  profileId: number
  profile: string
  type: string
  employeeStatus: string
  numberWorkDays: number
  numberWorkingDays: number
  numberDaysOnContractActivities: number
  numberDaysOffContractActivities: number
  numberDaysOffContractActivitiesOnUnworkedDays: number
  numberDaysInternalActivity: number
  numberDaysTrainerActivities: number
  numberDaysTrainingActivities: number
  numberDayLeaves: number
  staffingRate: number
  month: number
  year: number
}

export type Profile = {
  companyId: number
  profileId: number
  designation: string
  fonctionDesignation: string
  isInstructor: boolean | null
  ADR: string
  ADC: string
  offProdProfile: boolean
  functionNum: number
}

export type PalowanFromFitnet = {
  employeeId: number
  fullName: string
  companyName: string
  companyId: number
  staffingRate: number
  numberDaysOnContractActivities: number
  numberDaysOffContractActivities: number
  numberDaysInternalActivities: number
}
