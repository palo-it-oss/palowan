import { createApp } from 'vue'
import App from './App.vue'
// ROUTER
import router from './router'
// STORE
import { createPinia } from 'pinia'
// FORMKIT
import { plugin, defaultConfig } from '@formkit/vue'
import { generateClasses } from '@formkit/themes'
import formkitTheme from '@/formkit.theme'

// STYLES
import './assets/styles/tailwind.css'

const app = createApp(App)

app.use(createPinia())
app.use(router)
app.use(
  plugin,
  defaultConfig({
    config: {
      classes: generateClasses(formkitTheme)
    }
  })
)

app.mount('#app')
