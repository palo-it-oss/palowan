import { defineStore } from 'pinia'
import { ref } from 'vue'
// ROUTER
import { useRouter } from 'vue-router'
// COMPOSABLES
import { useFetchBackend } from '@/composables/useFetchBackend.composable'
// TYPES
import type { Profile } from 'types/user'

export const useUserStore = defineStore('user', () => {
  //--- INIT ---//

  //--- STATE ---//
  const token = ref('')
  const isAuthenticated = ref(true)
  const profile = ref<Profile>({})
  const picture = ref('')

  //--- ACTIONS ---//

  const signIn = async () => {
    const { data } = await useFetchBackend('/auth/signin').get().json()
    if (data.value) window.location.href = data.value.url
  }

  const signOut = async () => {
    const { data } = await useFetchBackend('/auth/signout').get().json()
    if (data.value) window.location.href = data.value.url
  }

  const getToken = async () => {
    const router = useRouter()
    const { data, error } = await useFetchBackend('/auth/token').get().json()
    if (error.value) {
      console.log(error.value)
      isAuthenticated.value = false
      router.push({ name: 'home' })
    }

    if (data.value) {
      console.log(data.value)
      token.value = data.value
      isAuthenticated.value = true
    }
  }

  const getUserProfile = async () => {
    const { data, error } = await useFetchBackend('/user/profile').get().json()
    if (error.value) console.log(error.value)
    if (data.value) profile.value = data.value
  }

  const getUserPicture = async () => {
    const { data, error } = await useFetchBackend('/user/profile/picture').get().blob()
    if (error.value) console.log(error.value)
    if (data.value) picture.value = URL.createObjectURL(data.value)
  }

  return {
    // STATE
    token,
    isAuthenticated,
    profile,
    picture,
    // ACTIONS
    signIn,
    signOut,
    getToken,
    getUserProfile,
    getUserPicture
  }
})
