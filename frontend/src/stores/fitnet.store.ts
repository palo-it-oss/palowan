import { defineStore } from 'pinia'
import { ref, computed, watchEffect } from 'vue'
import { useFetchBackend } from '@/composables/useFetchBackend.composable'
// TYPES
import type { Company, AvailabilitiesQuery, PalowanFromFitnet } from 'types/fitnet'

export const useFitnetStore = defineStore('fitnet', () => {
  //--- INIT ---//

  //--- STATE ---//
  const companies = ref<Company[]>([])
  const palowansFromFitnet = ref<PalowanFromFitnet[]>([])
  const isLoadingFitnet = ref(false)

  //--- ACTIONS ---//

  const getCompanies = async () => {
    const { data, error } = await useFetchBackend('/fitnet/companies').get().json()
    if (error.value) console.log(error.value)
    if (data.value) companies.value = data.value
  }

  const getAvailability = async (query: AvailabilitiesQuery) => {
    isLoadingFitnet.value = true
    const { data, error } = await useFetchBackend(
      `/fitnet/availability?startDate=${query.startDate}&endDate=${query.endDate}&companyId=${query.companyId}&onlyGardenView=${query.onlyGardenView}`
    )
      .get()
      .json()
    if (error.value) console.log(error.value)
    if (data.value) palowansFromFitnet.value = data.value
    isLoadingFitnet.value = false
  }

  const getAvailabilityByMonth = async (query: AvailabilitiesQuery) => {
    isLoadingFitnet.value = true
    const { data, error } = await useFetchBackend(
      `/fitnet/availability/month?startDate=${query.startDate}&companyId=${query.companyId}&onlyGardenView=${query.onlyGardenView}`
    )
      .get()
      .json()
    if (error.value) console.log(error.value)
    if (data.value) palowansFromFitnet.value = data.value
    isLoadingFitnet.value = false
  }

  const init = async () => {
    await getCompanies()
  }
  return {
    // STATE
    companies,
    palowansFromFitnet,
    isLoadingFitnet,
    // ACTIONS
    init,
    getAvailability,
    getAvailabilityByMonth
  }
})
