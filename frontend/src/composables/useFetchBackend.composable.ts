import { createFetch } from '@vueuse/core'
import { useUserStore } from '@/stores/user.store'

export const useFetchBackend = createFetch({
  baseUrl: '/api',
  options: {
    async beforeFetch({ options, cancel }) {
      /*       const userStore = useUserStore()

      if (!userStore.token) {
        console.log('no access token')
        userStore.isAuthenticated = false
        cancel()
      } */

      /*  options.headers = {
        ...options.headers,
        Accept: 'application/json',
        Authorization: `Bearer ${userStore.token}`
      } */

      return { options }
    },
    async afterFetch(context) {
      const userStore = useUserStore()
      if (context.response.status === 401) userStore.isAuthenticated = false
      return context
    }
  },
  fetchOptions: {
    mode: 'cors'
  }
})
