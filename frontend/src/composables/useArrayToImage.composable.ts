// This composable was written based on the package array-to-image
// Source: https://github.com/vaalentin/array-to-image

import { toValue, ref } from 'vue'

export const useArrayToImage = (
  array: Uint8ClampedArray,
  width: number | undefined = undefined,
  height: number | undefined = undefined
) => {
  const image = ref<HTMLImageElement>()
  const arrayValue = toValue(array)
  let widthValue = toValue(width)
  let heightValue = toValue(height)

  if (typeof widthValue === 'undefined' || typeof heightValue === 'undefined') {
    widthValue = heightValue = Math.sqrt(arrayValue.length / 4)
  }

  const canvas = document.createElement('canvas')
  const context = canvas.getContext('2d')

  canvas.width = widthValue
  canvas.height = heightValue

  const imageData = context!.createImageData(widthValue, heightValue)
  imageData.data.set(arrayValue)
  context!.putImageData(imageData, 0, 0)

  const data = canvas.toDataURL()

  // transform data to img
  image.value = document.createElement('img')
  image.value.src = data

  return { image }
}
