// The following Tailwind theme aspires to be a reproduction of the
// default optional Genesis CSS theme that ships with FormKit

export default {
  // Global styles apply to _all_ inputs with matching section keys
  global: {
    fieldset: 'tw-max-w-md tw-border tw-border-gray-400 tw-rounded tw-px-2 tw-pb-1',
    help: 'tw-text-xs tw-text-gray-500',
    inner:
      'formkit-disabled:tw-bg-gray-200 formkit-disabled:tw-cursor-not-allowed formkit-disabled:tw-pointer-events-none',
    input:
      'tw-appearance-none tw-bg-transparent focus:tw-outline-none focus:tw-ring-0 focus:tw-shadow-none',
    label: 'tw-block tw-mb-1 tw-font-bold tw-text-sm',
    legend: 'tw-font-bold tw-text-sm',
    loaderIcon: 'tw-inline-flex tw-items-center tw-w-4 tw-text-gray-600 tw-animate-spin',
    message: 'tw-text-red-500 tw-mb-1 tw-text-xs',
    messages: 'tw-list-none tw-p-0 tw-mt-1 tw-mb-0',
    outer: 'tw-mb-4 formkit-disabled:tw-opacity-50',
    prefixIcon:
      'tw-w-10 tw-flex tw-self-stretch tw-grow-0 tw-shrink-0 tw-rounded-tl tw-rounded-bl tw-border-r tw-border-gray-400 tw-bg-white tw-bg-gradient-to-b tw-from-transparent tw-to-gray-200 [&>svg]:tw-w-full [&>svg]:tw-max-w-[1em] [&>svg]:tw-max-h-[1em] [&>svg]:tw-m-auto',
    suffixIcon:
      'tw-w-7 tw-pr-3 tw-p-3 tw-flex tw-self-stretch tw-grow-0 tw-shrink-0 [&>svg]:tw-w-full [&>svg]:tw-max-w-[1em] [&>svg]:tw-max-h-[1em] [&>svg]:tw-m-auto'
  },

  // Family styles apply to all inputs that share a common family
  'family:box': {
    decorator:
      'tw-block tw-relative tw-h-5 tw-w-5 tw-mr-2 tw-rounded tw-bg-white tw-bg-gradient-to-b tw-from-transparent tw-to-gray-200 tw-ring-1 tw-ring-gray-400 peer-checked:tw-ring-primary tw-text-transparent peer-checked:tw-text-primary',
    decoratorIcon:
      'tw-flex tw-p-[3px] tw-w-full tw-h-full tw-absolute tw-top-1/2 tw-left-1/2 -tw-translate-y-1/2 -tw-translate-x-1/2',
    help: 'tw-mb-2 tw-mt-1.5',
    input:
      'tw-absolute tw-w-0 tw-h-0 tw-overflow-hidden tw-opacity-0 tw-pointer-events-none tw-peer',
    inner: '$remove:formkit-disabled:tw-bg-gray-200',
    label: '$reset tw-text-sm tw-text-gray-700 tw-mt-1 tw-select-none',
    wrapper: 'tw-flex tw-items-center tw-mb-1'
  },
  'family:button': {
    input:
      '$reset tw-inline-flex tw-items-center tw-bg-blue-600 tw-text-white tw-text-sm tw-font-normal tw-py-3 tw-px-6 tw-rounded focus-visible:tw-outline-2 focus-visible:tw-outline-blue-600 focus-visible:tw-outline-offset-2 formkit-disabled:tw-bg-gray-400 formkit-loading:before:tw-w-4 formkit-loading:before:tw-h-4 formkit-loading:before:tw-mr-2 formkit-loading:before:tw-border formkit-loading:before:tw-border-2 formkit-loading:before:tw-border-r-transparent formkit-loading:before:tw-rounded-3xl formkit-loading:before:tw-border-white formkit-loading:before:tw-animate-spin',
    wrapper: 'tw-mb-1',
    prefixIcon: '$reset tw-block tw-w-4 tw--ml-2 tw-mr-2 tw-stretch',
    suffixIcon: '$reset tw-block tw-w-4 tw-ml-2 tw-stretch'
  },
  'family:dropdown': {
    dropdownWrapper: 'tw-my-2 tw-w-full tw-shadow-lg tw-rounded [&::-webkit-scrollbar]:tw-hidden',
    emptyMessageInner:
      'tw-flex tw-items-center tw-justify-center tw-text-sm tw-p-2 tw-text-center tw-w-full tw-text-gray-500 [&>span]:tw-mr-3 [&>span]:tw-ml-0',
    inner:
      'tw-max-w-md tw-relative tw-flex tw-ring-1 tw-ring-gray-400 focus-within:ring-primary focus-within:tw-ring-2 tw-rounded tw-mb-1 formkit-disabled:focus-within:tw-ring-gray-400 formkit-disabled:focus-within:tw-ring-1 [&>span:first-child]:focus-within:tw-text-primary',
    input: 'tw-w-full tw-px-3 tw-py-2',
    listbox: 'tw-bg-white tw-shadow-lg tw-rounded tw-overflow-hidden',
    listboxButton: 'tw-flex tw-w-12 tw-self-stretch tw-justify-center tw-mx-auto',
    listitem:
      'tw-pl-7 tw-relative hover:tw-bg-gray-300 data-[is-active="true"]:tw-bg-gray-300 data-[is-active="true"]:aria-selected:tw-bg-blue-600 aria-selected:tw-bg-blue-600 aria-selected:tw-text-white',
    loaderIcon: 'tw-ml-auto',
    loadMoreInner:
      'tw-flex tw-items-center tw-justify-center tw-text-sm tw-p-2 tw-text-center tw-w-full text-primary formkit-loading:tw-text-gray-500 tw-cursor-pointer [&>span]:tw-mr-3 [&>span]:tw-ml-0',
    option: 'tw-p-2.5',
    optionLoading: 'tw-text-gray-500',
    placeholder: 'tw-p-2.5 tw-text-gray-400',
    selector: 'tw-flex tw-w-full tw-justify-between tw-items-center [&u]',
    selectedIcon: 'tw-block tw-absolute tw-top-1/2 tw-left-2 tw-w-3 tw--translate-y-1/2',
    selectIcon:
      'tw-flex tw-box-content tw-w-4 tw-px-2 tw-self-stretch tw-grow-0 tw-shrink-0 [&>svg]:tw-w-[1em]'
  },
  'family:text': {
    inner:
      'tw-flex tw-items-center tw-max-w-md tw-ring-1 tw-ring-gray-400 focus-within:tw-ring-primary focus-within:tw-ring-2 [&>label:first-child]:focus-within:tw-text-primary tw-rounded tw-mb-1',
    input:
      'tw-w-full tw-px-3 tw-py-2 tw-border-none tw-text-base tw-text-gray-700 tw-placeholder-gray-400'
  },
  'family:date': {
    inner:
      'tw-flex tw-items-center tw-max-w-md tw-ring-1 tw-ring-gray-400 focus-within:tw-ring-primary focus-within:tw-ring-2 [&>label:first-child]:focus-within:tw-text-primary tw-rounded tw-mb-1',
    input: 'tw-w-full tw-px-3 tw-py-2 tw-border-none tw-text-gray-700 tw-placeholder-gray-400'
  },

  // Specific styles apply only to a given input type
  color: {
    inner:
      'tw-flex tw-max-w-[5.5em] tw-w-full formkit-prefix-icon:tw-max-w-[7.5em] formkit-suffix-icon:formkit-prefix-icon:tw-max-w-[10em]',
    input:
      '$reset tw-appearance-none tw-w-full tw-cursor-pointer tw-border-none tw-rounded tw-p-0 tw-m-0 tw-bg-transparent [&::-webkit-color-swatch-wrapper]:tw-p-0 [&::-webkit-color-swatch]:tw-border-none',
    suffixIcon: 'tw-min-w-[2.5em] tw-pr-0 tw-pl-0 tw-m-auto'
  },
  file: {
    fileItem: 'tw-flex tw-items-center tw-text-gray-800 tw-mb-1 last:tw-mb-0',
    fileItemIcon: 'tw-w-4 tw-mr-2 tw-shrink-0',
    fileList:
      'tw-shrink tw-grow tw-peer tw-px-3 tw-py-2 formkit-multiple:data-[has-multiple="true"]:tw-mb-6',
    fileName: 'tw-break-all tw-grow tw-text-ellipsis',
    fileRemove:
      'tw-relative tw-z-[2] tw-ml-auto tw-text-[0px] hover:tw-text-red-500 tw-pl-2 peer-data-[has-multiple=true]:tw-text-sm peer-data-[has-multiple=true]:tw-text-primary peer-data-[has-multiple=true]:tw-ml-3 peer-data-[has-multiple=true]:tw-mb-2 formkit-multiple:tw-bottom-[0.15em] formkit-multiple:tw-pl-0 formkit-multiple:tw-ml-0 formkit-multiple:tw-left-[1em] formkit-multiple:formkit-prefix-icon:tw-left-[3.75em]',
    fileRemoveIcon: 'tw-block tw-text-base tw-w-3 tw-relative tw-z-[2]',
    inner: 'tw-relative tw-max-w-md tw-cursor-pointer formkit-multiple:[&>button]:tw-absolute',
    input:
      'tw-cursor-pointer tw-text-transparent tw-absolute tw-top-0 tw-right-0 tw-left-0 tw-bottom-0 tw-opacity-0 z-[2]',
    noFiles: 'tw-flex tw-w-full tw-items-center tw-px-3 tw-py-2 tw-text-gray-400',
    noFilesIcon: 'tw-w-4 tw-mr-2'
  },
  radio: {
    decorator: 'tw-rounded-full',
    decoratorIcon: 'tw-w-5 p-[5px]'
  },
  range: {
    inner: '$reset tw-flex tw-items-center tw-max-w-md',
    input: '$reset tw-w-full tw-mb-1 tw-h-2 tw-p-0 tw-rounded-full',
    prefixIcon:
      '$reset tw-w-4 tw-mr-1 tw-flex tw-self-stretch tw-grow-0 tw-shrink-0 [&>svg]:tw-max-w-[1em] [&>svg]:tw-max-h-[1em] [&>svg]:tw-m-auto',
    suffixIcon:
      '$reset tw-w-4 tw-ml-1 tw-flex tw-self-stretch tw-grow-0 tw-shrink-0 [&>svg]:tw-max-w-[1em] [&>svg]:tw-max-h-[1em] [&>svg]:tw-m-auto'
  },
  select: {
    inner:
      'tw-flex tw-relative tw-max-w-md tw-items-center tw-rounded tw-mb-1 tw-ring-1 tw-ring-gray-400 focus-within:tw-ring-primary focus-within:tw-ring-2 [&>span:first-child]:focus-within:tw-text-primary',
    input:
      'tw-w-full tw-pl-3 tw-pr-8 tw-py-2 tw-border-none tw-text-base tw-text-gray-700 tw-placeholder-gray-400 formkit-multiple:tw-p-0 data-[placeholder="true"]:tw-text-gray-400 formkit-multiple:data-[placeholder="true"]:tw-text-inherit',
    selectIcon:
      'tw-flex tw-p-[3px] tw-shrink-0 tw-w-5 tw-mr-2 -tw-ml-[1.5em] tw-h-full tw-pointer-events-none [&>svg]:tw-w-[1em]',
    option: 'formkit-multiple:tw-p-3 formkit-multiple:tw-text-sm tw-text-gray-700'
  },
  textarea: {
    inner:
      'tw-flex tw-max-w-md tw-rounded tw-mb-1 tw-ring-1 tw-ring-gray-400 focus-within:tw-ring-primary [&>label:first-child]:focus-within:tw-text-primary',
    input:
      'tw-block tw-w-full tw-h-32 tw-px-3 tw-py-3 tw-border-none tw-text-base tw-text-gray-700 tw-placeholder-gray-400 focus:tw-shadow-outline'
  }
}
