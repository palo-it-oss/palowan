import { createRouter, createWebHistory } from 'vue-router'
/* import { registerGuard } from './Guard' */
// STORE
import { useUserStore } from '@/stores/user.store'
// VIEWS
import Home from '@/views/HomeView.vue'
import Dashboard from '@/views/DashBoard.vue'
import PersonalMaps from '@/views/PersonalMaps.vue'
import Profile from '@/views/UserProfile.vue'
import UserSearch from '@/views/UserSearch.vue'

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard,
      meta: {
        requiresAuth: true
      },
      children: [
        {
          path: '',
          name: 'search',
          component: UserSearch
        },
        {
          path: 'profile',
          name: 'profile',
          component: Profile
        },
        {
          path: 'personal-maps',
          name: 'personalMaps',
          component: PersonalMaps
        }
      ]
    }
  ]
})

router.beforeEach(async (to, from) => {
  const userStore = useUserStore()
  if (to.meta.requiresAuth && !userStore.isAuthenticated) return { name: 'home' }
})

export default router
