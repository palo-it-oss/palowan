/** @type {import('tailwindcss').Config} */
import formKitVariants from '@formkit/themes/tailwindcss'
import daisyUi from 'daisyui'
import daisyUiThemes from 'daisyui/src/theming/themes'

export default {
  mode: 'jit',
  prefix: 'tw-',
  content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    extend: {}
  },
  plugins: [daisyUi, formKitVariants],
  daisyui: {
    themes: [
      {
        light: {
          ...daisyUiThemes['[data-theme=light]'],
          primary: '#05cc98',
          secondary: '#ef8035'
        }
      },
      {
        dark: {
          ...daisyUiThemes['[data-theme=dark]'],
          primary: '#05cc98',
          secondary: '#ef8035'
        }
      }
    ]
  }
}
