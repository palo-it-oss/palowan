# Instruction

This is a fullstack JS app, build on top of NestJS (backend), VueJS (frontend), MSAL (MS auth library).
The app is deployed on a small server on OVH in France.

## Development

First launch
RUN `npm run infra-up:dev:install`

Then :
RUN `npm run infra-up:dev`

# Deployment

RUN `npm run build:deploy`
